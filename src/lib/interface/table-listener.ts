import { PaginationListener } from '@faab/pagination-lib';

export interface TableListener extends PaginationListener {
  onItemClick(ob: any): void;
}
