import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { TableLibComponent } from './component/table-lib.component';
import { CommonModule, DatePipe } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PaginationLibModule } from '@faab/pagination-lib';

@NgModule({
  declarations: [
    TableLibComponent
  ],
  imports: [
    TranslateModule,
    CommonModule,
    NgbModule,
    PaginationLibModule
  ],
  providers: [
    DatePipe
  ],
  exports: [TableLibComponent]
})
export class TableLibModule { }
