import { DatePipe, Location } from '@angular/common';
import { TableListener } from './../interface/table-listener';
import { Updatable } from '@faab/base-lib';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '@faab/http-lib';
import { StorageService } from '@faab/storage-lib';
import { Component, TemplateRef, Input, Output, EventEmitter } from '@angular/core';
import { PaginationLibComponent, PaginationService } from '@faab/pagination-lib';
import { AvailableService, PaginationTheadCellComponentData, PaginationEvent } from '@faab/pagination-lib';
import { Subscription } from 'rxjs';
import { LanguageLibService } from '@faab/language-lib';

@Component({
  selector: 'lib-table-lib',
  templateUrl: './table-lib.component.html',
  styles: []
})
export class TableLibComponent extends PaginationLibComponent {

  @Input() sector: string;
  @Input() controls: TemplateRef<any>;
  @Input() isClickable: boolean = false;

  @Output() itemClick: EventEmitter<Updatable> = new EventEmitter<Updatable>();

  public listener: TableListener;

  protected _subscription_onItemClick: Subscription;

  constructor(
    protected http: HttpService,
    protected storage: StorageService,
    protected location: Location,
    protected router: Router,
    protected availableService: AvailableService,
    protected activatedRoute: ActivatedRoute,
    protected language: LanguageLibService,
    protected datePipe: DatePipe,
    protected pagination: PaginationService
  ) {
    super(http, storage, location, router, availableService, activatedRoute, language, datePipe, pagination);
  }

  onInit() {
    super.onInit();
    this.onPagerInit();
  }

  onDestroy(): void {
    super.onDestroy();
    this.checkSubscription(this._subscription_onItemClick);
  }

  onClick(item: Updatable) {
    if (this.isClickable) {
      this.itemClick.emit(item);
    }
  }

  public setData(items: Updatable[]) {
    if (this.sector) {
      const arr = [];
      let prevMonth = null;
      for (let i = 0; i < items.length; i++) {
        if (items[i][this.sector]) {
          const date = new Date(items[i][this.sector]);
          const month = date.getMonth();
          if (prevMonth !== month) {
            prevMonth = month;
            arr.push({sector: true, value: items[i][this.sector]});
            i--;
            continue;
          }
        }
        arr.push(items[i]);
      }
      this.checkListener(arr);
    } else {
      this.checkListener(items);
    }
  }

  private checkListener(items: any) {
    if (this.listener) {
      this.listener.onDataUpdate(items);
    }
    this._items = items;
  }

  public getFormat(value): string {
    const formats = value.split('::');
    return formats[formats.length - 1];
  }

  public getSectorName(item) {
    return item.value + this.getLangOffset();
  }

  public isSector(item) {
    try {
      if (item.sector) {
        return true;
      }
    } catch (e) {
      return false;
    }
  }


  public getCountColumn() {
    return this.controls ? this.keys.length + 1 : this.keys.length;
  }

  public getLocalized(key: string) {
    try {
      return this.attributes[key].localized;
    } catch (e) {
      return false;
    }
  }

  public getStyleClass(key: string, item?: any) {
    let cls = {};
    if (item) {
      cls = {'table-item-deleted': !isNaN(item.whenDeleted)};
    }

    try {
      Object.assign(cls, this.attributes[key].class);
    } catch (e) {

    }

    return cls;
  }

  public getImage(k: string, item: any) {
    if (item[k]) {
      return item[k];
    }

    /* let avaID: string;

    try {
      const user: User = item as User;
      user.properties.forEach(value => {
        if (value.property.name === 'avatar') {
          avaID = value.value;
        }
      });
    } catch (e) {

    }
    return avaID;
 */
  }

  public getCount() {
    try {
      return this._items.length;
    } catch (e) {
      return 0;
    }
  }

  public getPaginationTheadCellComponentData(key: string): PaginationTheadCellComponentData {
    if (typeof key !== 'string') {
      return;
    }
    return super.getPaginationTheadCellComponentData(key);
  }

  public onFilterChange(e: PaginationEvent): void {
    if (!(e instanceof PaginationEvent)) {
      return;
    }
    return super.onFilterChange(e);
  }

  protected setInitialization(controller: any): void {
    super.setInitialization(controller);
    if (!controller) {
      return;
    }

    this.isClickable = controller.isClickable();
    this.checkSubscription(this._subscription_onItemClick);
    this._subscription_onItemClick = this.itemClick.subscribe((v) => {
      controller.itemClick(v);
    });
    this.controls = controller.getControls();
  }

  protected getUrl(): string {
    const path: string = this.location.path(),
      path_arr: string[] = path.split('?');

    return (typeof path_arr[0] !== 'string') ? path : path_arr[0];
  }


}
